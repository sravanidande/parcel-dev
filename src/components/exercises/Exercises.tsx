import {
  Grid,
  Paper,
  Typography,
  List,
  ListItem,
  ListItemText,
} from '@material-ui/core'
import React from 'react'
import { Exercise } from './store'

const styles = {
  Paper: {
    padding: 20,
    marginBottom: 10,
    marginTop: 10,
    height: 500,
    overflowY: 'auto',
  },
}

interface ExercisesProps {
  readonly exercises: ReadonlyArray<Exercise>
  readonly category: string
}

export const Exercises: React.FC<ExercisesProps> = ({
  exercises,
  category,
}) => {
  return (
    <Grid container spacing={2}>
      <Grid item sm>
        <Paper style={styles.Paper}>
          {exercises.map(([muscle, exercises]) =>
            !category || category === muscle ? (
              <>
                <Typography
                  variant="subtitle1"
                  style={{ textTransform: 'capitalize' }}
                >
                  {muscle}
                </Typography>
                <List component="ul">
                  {exercises.map(exercise => {
                    return (
                      <ListItem button>
                        <ListItemText primary={exercise.title} />
                      </ListItem>
                    )
                  })}
                </List>
              </>
            ) : null,
          )}
        </Paper>
      </Grid>
      <Grid item sm>
        <Paper style={styles.Paper}>
          <>
            <Typography variant="h4">welcome !!😊🙏</Typography>
            <Typography variant="subtitle1">
              please select an exercise from the list on the left
            </Typography>
          </>
        </Paper>
      </Grid>
    </Grid>
  )
}
