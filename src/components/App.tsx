import React from 'react'
import { Exercises, exercises, muscles } from './exercises'
import {
  evenFilter,
  findIndex,
  Footer,
  Header,
  largestNumber,
  reverseArray,
  squareArray,
  sumOfAll,
} from './layouts'

const getExercisesByMuscles = () => {
  return Object.entries(
    exercises.reduce((exercises: any, exercise: any) => {
      const { muscles } = exercise
      exercises[muscles] = exercises[muscles]
        ? [...exercises[muscles], exercise]
        : [exercise]
      return exercises
    }, {}),
  )
}

const exercisesList = getExercisesByMuscles()

export const App: React.FC = () => {
  const [category, setCategory] = React.useState('')

  const handleTabChange = (newCategory: string) => {
    setCategory(newCategory)
  }
  return (
    <>
      <Header />
      <Exercises exercises={exercisesList} category={category} />
      <Footer
        muscles={muscles}
        category={category}
        onTabChange={handleTabChange}
      />
    </>
  )
}

console.log(squareArray([1, 2, 3]))
console.log(evenFilter([1, 2, 3, 4, 5, 6]))
console.log(sumOfAll([1, 2, 3, 4, 5, 6]))
console.log(findIndex([1, 2, 3, 4], 4))
console.log(reverseArray([1, 2, 3, 4]))
console.log(largestNumber([1, 2, 3, 4]))
