import { Tab, Tabs } from '@material-ui/core'
import React from 'react'
import { Exercise } from '../exercises'

interface FooterProps {
  readonly muscles: ReadonlyArray<string>
  onTabChange(index: any): void
  category: string
}

export const Footer: React.FC<FooterProps> = ({
  muscles,
  category,
  onTabChange,
}) => {
  const index = category
    ? muscles.findIndex(muscle => category === muscle) + 1
    : 0
  const onIndexSelect = (e: any, index: any) => {
    onTabChange(index === 0 ? '' : muscles[index - 1])
  }
  return (
    <Tabs
      value={index}
      indicatorColor="primary"
      textColor="primary"
      onChange={onIndexSelect}
    >
      <Tab label="All" />
      {muscles.map((muscle, index) => (
        <Tab key={index} label={muscle} />
      ))}
    </Tabs>
  )
}
