import React from 'react'
import { Button, Typography } from '@material-ui/core'

export const Counter: React.FC = () => {
  const [counter, setCount] = React.useState(0)
  return (
    <>
      <Button
        variant="contained"
        color="primary"
        onClick={() => setCount(counter + 1)}
      >
        +
      </Button>
      <Typography variant="h4">count:{counter}</Typography>
      <Button
        variant="contained"
        color="secondary"
        onClick={() => setCount(counter - 1)}
      >
        -
      </Button>
    </>
  )
}
