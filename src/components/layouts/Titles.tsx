import React from 'react'

interface TitleProps {
  readonly title: string
}

export const Title: React.FC<TitleProps> = ({ title }) => {
  return <li>{title}</li>
}

export const Titles: React.FC = () => {
  const [titles, setTitles] = React.useState([])
  React.useEffect(() => {
    fetch('http://localhost:3000/todos')
      .then(res => res.json())
      .then(data => setTitles(data.map(item => item.title)))
      .catch(err => console.log(err))
  }, [])

  return (
    <ul>
      {titles.map((item, index) => (
        <Title key={index} title={item} />
      ))}
    </ul>
  )
}
