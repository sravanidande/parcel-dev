import {
  squareArray,
  evenFilter,
  sumOfAll,
  largestNumber,
  findIndex,
  reverseArray,
} from './index'

it('test for squareArray', () => {
  expect(squareArray([1, 2, 3, 4])).toEqual([1, 4, 9, 16])
  expect(squareArray([0, 1])).toEqual([0, 1])
  expect(squareArray([])).toEqual([])
})

it('test for even number in an array', () => {
  expect(evenFilter([1, 2, 3, 4, 5, 6])).toEqual([2, 4, 6])
  expect(evenFilter([1, 3, 5, 2])).toEqual([2])
  expect(evenFilter([1, 3, 5])).toEqual([])
  expect(evenFilter([0, 2, 4])).toEqual([0, 2, 4])
  expect(evenFilter([])).toEqual([])
})

it('test for sum of all numbers in an array', () => {
  expect(sumOfAll([1, 2, 3, 4])).toEqual(10)
  expect(sumOfAll([1, 0])).toEqual(1)
  expect(sumOfAll([])).toEqual(0)
})

it('test for largest number in an array', () => {
  expect(largestNumber([1, 2, 3, 4, 5])).toEqual(5)
  expect(largestNumber([5, 4, 16, 8, 32])).toEqual(32)
  expect(largestNumber([-2, -1, 0, 1])).toEqual(1)
  expect(largestNumber([])).toEqual(undefined)
})

it('test for find index of given element in an array', () => {
  expect(findIndex([1, 2, 3, 4, 5], 4)).toEqual(3)
  expect(findIndex([1, 2, 3], 4)).toEqual(-1)
  expect(findIndex([1, 2, 3], 1)).toEqual(0)
  expect(findIndex([], 1)).toEqual(-1)
})

it('test for reversing an array', () => {
  expect(reverseArray([1, 2, 4, 5])).toEqual([5, 4, 2, 1])
  expect(reverseArray([-2, -1, 0, 1])).toEqual([1, 0, -1, -2])
  expect(reverseArray([])).toEqual([])
})
