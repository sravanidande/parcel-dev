// tslint:disable: prefer-for-of

export * from './Footer'
export * from './Header'
export * from './Counter'
export * from './Titles'

export const squareArray = (arr: number[]): number[] => {
  const result = []
  for (let i = 0; i < arr.length; i += 1) {
    result.push(arr[i] * arr[i])
  }
  return result
}

export const evenFilter = (arr: number[]): number[] => {
  const result = []
  for (let i = 0; i < arr.length; i += 1) {
    if (arr[i] % 2 === 0) {
      result.push(arr[i])
    }
  }
  return result
}

export const sumOfAll = (arr: number[]): number => {
  let sum = 0
  for (let i = 0; i < arr.length; i += 1) {
    sum = sum + arr[i]
  }
  return sum
}

export const findIndex = (arr: number[], value: number): number => {
  for (let i = 0; i < arr.length; i += 1) {
    if (arr[i] === value) {
      return i
    }
  }
  return -1
}

export const reverseArray = (arr: number[]): number[] => {
  const result = []
  for (let i = arr.length - 1; i >= 0; i -= 1) {
    result.push(arr[i])
  }
  return result
}

export const largestNumber = (arr: number[]): number => {
  let max = arr[0]
  for (let i = 1; i < arr.length; i += 1) {
    if (max < arr[i]) {
      max = arr[i]
    }
  }
  return max
}
