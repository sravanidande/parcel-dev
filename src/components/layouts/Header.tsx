import { AppBar, Toolbar, Typography } from '@material-ui/core'
import React from 'react'

export const Header: React.FC = () => {
  return (
    <AppBar position="static">
      <Toolbar>
        <Typography variant="h4" color="inherit">
          Exercise Database
        </Typography>
      </Toolbar>
    </AppBar>
  )
}
