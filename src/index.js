import React from 'react'
import { render } from 'react-dom'

import { App } from './components'

render(React.createElement(App), document.getElementById('root'))
